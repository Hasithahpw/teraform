import boto3
import requests
import sys

s3 = boto3.client('s3')
file_key = sys.argv[1]

print("generating presinged URL.......")

response = s3.generate_presigned_post('hpw-demo',file_key,Fields=None,Conditions=None,ExpiresIn=3600)
 
print(response)

print("Uploading the file     .......")
fin = open(file_key, "rb")
files = {'file':fin}
try:
    r = requests.post(response['url'], data=response['fields'], files=files)
    print(r.status_code)
finally:
    fin.close()
