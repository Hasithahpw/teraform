provider "aws" {
  profile = "default"
  region  = "ap-southeast-1"
}

############## Create S3 Bucket for logs ##############
resource "aws_s3_bucket" "hpw_log_bucket" {
  bucket = "hpw-log-bucket"
  acl    = "public-read-write"
}

############## Create S3 Bucket ##############
resource "aws_s3_bucket" "hpw-demo" {
  bucket = "hpw-demo"
  acl = "private"
}

############## Block public access ##############
resource "aws_s3_bucket_public_access_block" "public_access_block" {
  bucket = aws_s3_bucket.hpw-demo.id
  block_public_acls = true
  ignore_public_acls = true
  block_public_policy = true
  restrict_public_buckets = true
}

############## Upload first file ##############
resource "aws_s3_bucket_object" "hello-world" {
  bucket = aws_s3_bucket.hpw-demo.id
  key    = "profile"
  acl    = "public-read"  # or can be "public-read"
  source = "welcome"
  etag = filemd5("welcome")
}

############## Define IAM role for EC2 to access S3 ##############
resource "aws_iam_role" "ec2_s3_access_role" {
  name               = "ec2_s3_access_role"
  assume_role_policy = "${file("ec2rolepolicy.json")}"
}

############## Define IAM role policy ##############
resource "aws_iam_role_policy" "hpw-demo-policy" {
  name        = "hpw-demo-policy"
  role        = "${aws_iam_role.ec2_s3_access_role.id}"
  policy      = "${file("iam-role-policy.json")}"
}

############## Define EC2 instance profile ##############
resource "aws_iam_instance_profile" "test_profileA" {
  name  = "test_profileA"
  role = "${aws_iam_role.ec2_s3_access_role.name}"
}

############## Cretae VPC ##############
resource "aws_vpc" "my_vpc" {
  cidr_block       = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "My VPC"
  }
}

############## Create public subnets ##############
resource "aws_subnet" "public_ap_southeast_1a" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = "10.0.0.0/24"
  availability_zone = "ap-southeast-1a"

  tags = {
    Name = "Public Subnet ap-southeast-1a"
  }
}

resource "aws_subnet" "public_ap_southeast_1b" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "ap-southeast-1b"

  tags = {
    Name = "Public Subnet ap-southeast-1b"
  }
}

resource "aws_subnet" "public_ap_southeast_1c" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "ap-southeast-1c"

  tags = {
    Name = "Public Subnet ap-southeast-1c"
  }
}

############## Cretae IGW ##############
resource "aws_internet_gateway" "my_vpc_igw" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "My VPC - Internet Gateway"
  }
}

############## Routing table ##############
resource "aws_route_table" "my_vpc_public" {
    vpc_id = aws_vpc.my_vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.my_vpc_igw.id
    }

    tags = {
        Name = "Public Subnets Route Table for My VPC"
    }
}

############## Cretae Route table associantions ##############
resource "aws_route_table_association" "my_vpc_ap_southeast_1a_public" {
    subnet_id = aws_subnet.public_ap_southeast_1a.id
    route_table_id = aws_route_table.my_vpc_public.id
}

resource "aws_route_table_association" "my_vpc_ap_southeast_1b_public" {
    subnet_id = aws_subnet.public_ap_southeast_1b.id
    route_table_id = aws_route_table.my_vpc_public.id
}

resource "aws_route_table_association" "my_vpc_ap_southeast_1c_public" {
    subnet_id = aws_subnet.public_ap_southeast_1c.id
    route_table_id = aws_route_table.my_vpc_public.id
}

############## Cretae Security group from EC2 ##############
resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "Allow HTTP inbound connections"
  vpc_id = aws_vpc.my_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Allow HTTP Security Group"
  }
}

############## Define EC2 launch configuration ##############
resource "aws_launch_configuration" "web" {
  name_prefix = "web-"

  image_id = "ami-00b8d9cb8a7161e41" # Amazon Linux 2 AMI (HVM), SSD Volume Type
  instance_type = "t2.micro"
  key_name = "hasitha"
  
  iam_instance_profile = "${aws_iam_instance_profile.test_profileA.name}"

  security_groups = [ aws_security_group.allow_http.id ]
  associate_public_ip_address = true

  user_data = <<USER_DATA
#!/bin/bash
yum update
yum -y install automake fuse fuse-devel gcc-c++ git libcurl-devel libxml2-devel make openssl-devel
yum -y install httpd
systemctl start httpd.service
systemctl enable httpd.service
yum -y install php
systemctl restart httpd.service
git clone https://github.com/s3fs-fuse/s3fs-fuse.git
cd  s3fs-fuse
./autogen.sh 
./configure --prefix=/usr --with-openssl
make 
make install
mkdir -p /var/demo
s3fs -o iam_role="ec2_s3_access_role" -o url="https://s3-ap-southeast-1.amazonaws.com" -o endpoint=ap-southeast-1 -o dbglevel=info -o curldbg -o allow_other -o gid=$(id -g apache) -o use_cache=/tmp hpw-demo /var/demo
cd /var/www/html
git clone https://gitlab.com/Hasithahpw/hello.git
  USER_DATA

  lifecycle {
    create_before_destroy = true
  }
}

############## Cretae security group for ALB ##############
resource "aws_security_group" "alb" {
  name        = "terraform_alb_security_group"
  description = "Terraform load balancer security group"
  vpc_id      = aws_vpc.my_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all outbound traffic.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "alb-security-group"
  }
}

############## Cretae ASG ##############
resource "aws_autoscaling_group" "asg-sample" {
  launch_configuration = aws_launch_configuration.web.name
  vpc_zone_identifier = [aws_subnet.public_ap_southeast_1a.id,aws_subnet.public_ap_southeast_1b.id,]
  min_size = 2
  max_size = 4

  tag {
    key                 = "Name"
    value               = "web-app"
    propagate_at_launch = true
  }
}
############## Cretae ALB ##############
resource "aws_alb" "alb" {  
  name            = "alb"  
  subnets         = [
    aws_subnet.public_ap_southeast_1a.id,
    aws_subnet.public_ap_southeast_1b.id,
    aws_subnet.public_ap_southeast_1c.id
  ]
  security_groups = [aws_security_group.alb.id]
  internal           = false
  load_balancer_type = "application"  
  tags = {    
    Name    = "alb"    
  }
  access_logs {
    bucket = aws_s3_bucket.hpw_log_bucket.bucket
    prefix = "alb"
    enabled = true
  }
}

############## Cretae ALB listner ##############
resource "aws_alb_listener" "alb_listener" {  
  load_balancer_arn = "${aws_alb.alb.arn}"  
  port              = "80"  
  protocol          = "HTTP"
  
  default_action {    
    target_group_arn = "${aws_alb_target_group.alb_target_group.arn}"
    type             = "forward"  
  }
}

############## Cretae ALB listener rule ##############
resource "aws_alb_listener_rule" "listener_rule" {
  depends_on   = ["aws_alb_target_group.alb_target_group"]  
  listener_arn = "${aws_alb_listener.alb_listener.arn}"  
  priority     = 100  
  action {    
    type             = "forward"    
    target_group_arn = "${aws_alb_target_group.alb_target_group.id}"  
  }   
  condition {    
    path_pattern {
      values = ["/hello/*"]
    }
  }
  
}

############## Cretae ALB target group ##############
resource "aws_alb_target_group" "alb_target_group" {  
  name     = "alb-target-group"  
  port     = 80 
  protocol = "HTTP"  
  vpc_id   = aws_vpc.my_vpc.id  
  tags = {    
    name = "demo-alb_target_group"    
  }   
 
  health_check {    
    healthy_threshold   = 3    
    unhealthy_threshold = 10    
    timeout             = 5    
    interval            = 10    
    path                = "/hello/"    
    port                = 80  
  }
}

############## Autoscalling attachment ##############
resource "aws_autoscaling_attachment" "svc_asg_external2" {
  alb_target_group_arn   = "${aws_alb_target_group.alb_target_group.arn}"
  autoscaling_group_name = "${aws_autoscaling_group.asg-sample.id}"
}

############## GET ALB URL ##############
output "alb_dns_name" {
  value = aws_alb.alb.dns_name
}

############## scale up ##############

resource "aws_autoscaling_policy" "demo_scalling_policy" {
  name                   = "demo_scalling_policy"
  policy_type            = "TargetTrackingScaling"
  autoscaling_group_name = aws_autoscaling_group.asg-sample.name
 
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
 
    target_value = 40.0
  }
}
