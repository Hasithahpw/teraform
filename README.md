# Static web Hosting at AWS using Terraform

Prerequisite:

install aws cli

configure aws keys

generate aws ssh key pair named "hasitha"

install terraform

install python, python-pip, boto3



Run below commands:

aws configure

terraform init

terraform validate

terraform apply  --> setup environment

terraform  destroy  --> clean up environment


